import RPi.GPIO as GPIO, time

def startBoard():
    GPIO.setmode(GPIO.BOARD)

def setIn(pin):
    GPIO.setup(pin, GPIO.IN)

def setOut(pin):
    GPIO.setup(pin, GPIO.OUT)

def cleanGPIO():
    GPIO.cleanup()

def setHigh(pin):
    GPIO.output(pin, GPIO.HIGH)

def setLow(pin):
    GPIO.output(pin, GPIO.LOW)

def blinkLED(pin, s):
    setHigh(pin)
    time.sleep(s)
    setLow(pin)

def setPinsOut(pins):
    for i in range(0, len(pins)):
        setOut(pins[i])

def setPinsIn(pins):
    for i in range(0, len(pins)):
        setIn(pins[i])

def quickStart():
    GPIO.setwarnings(False)
    cleanGPIO()
    startBoard()

def setAllHigh(pins):
    for i in range(0, len(pins)):
        setHigh(pins[i])

def setAllLow(pins):
    for i in range(0, len(pins)):
        setLow(pins[i])

def readPin(pin):
    return GPIO.input(pin)
