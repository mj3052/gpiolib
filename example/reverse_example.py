from gpiolib import *

# Example code with 4 LEDs on pin 10,11,12,13
# By emilstahl - http://emilstahl.dk

# Start
quickStart()

# Set LED pins
LEDs = [10,11,12,13]
setPinsOut(LEDs)

# Define LED pins to color (we dont use them right now)
red = 10
yellow = 11
green = 12
red2 = 13

# Set times
blink = 0.03
delay = 0.5

# While loop
while True:
        # Blink LEDs
        for i in range(0,len(LEDs)):
                blinkLED(LEDs[i],blink)
        # Delay
        time.sleep(delay)

        # Blink LEDs reverse
        for i in range(0,len(LEDs)):
                blinkLED(LEDs[::-1][i],blink)

        # Delay
        time.sleep(delay)